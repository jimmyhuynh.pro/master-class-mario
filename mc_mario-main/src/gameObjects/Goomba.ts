import LogoScene from "../scenes/LogoScene";

export default class Goomba extends Phaser.GameObjects.Sprite {
    public scene:LogoScene;

    public body: Phaser.Physics.Arcade.Body;

    constructor(scene:LogoScene, x:number, y:number, texture: string, frame: number) 
    {
        super(scene, x, y, texture, frame);

        this.scene = scene;
        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);
        this.setDepth(1)

        this.body.setCollideWorldBounds(true).setSize(12, 16, true);
    }

        public preUpdate(time:number, delta:number):void {

            super.preUpdate(time, delta);
            this.body.setVelocityX(-10);
    
        
        }

    }
