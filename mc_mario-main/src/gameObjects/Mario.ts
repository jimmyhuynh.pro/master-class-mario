import LogoScene from "../scenes/LogoScene";

export default class Mario extends Phaser.GameObjects.Sprite
{
    public scene:LogoScene;

    public body: Phaser.Physics.Arcade.Body;

    public keys: Phaser.Types.Input.Keyboard.CursorKeys;

    constructor(scene:LogoScene, x:number, y:number, texture: string, frame: number) 
    {
        super(scene, x, y, texture, frame);

        this.scene = scene;
        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);
        this.setDepth(1)

        this.body.setCollideWorldBounds(true).setSize(12, 16, true);

        this.keys = this.scene.input.keyboard.createCursorKeys();
    }

    public preUpdate(time:number, delta:number):void {

        super.preUpdate(time, delta);

        if(this.keys.left.isDown) {
            this.body.setVelocityX(-100);
            this.anims.play('walk');
        }
        if(this.keys.left.isUp) {
            this.body.setVelocityX(0);
            this.anims.stop();
            
        }
        if(this.keys.right.isDown) {
            this.body.setVelocityX(100);
            this.anims.play('walk');
        }
        if(this.keys.up.isDown) {
            this.body.setVelocityY(-100);
            this.anims.play('jump')
        }
    }
}