import { Scene } from 'phaser';

// import needed assets files
import logo from '../assets/graphics/logo.png';
import { WIDTH } from '../constant/config';
import { HEIGHT } from '../constant/config';
import mario from '../assets/graphics/mario-atlas.png';
import Mario from '../gameObjects/Mario';
import tiles from '../assets/graphics/tiles.png';
import map1 from '../assets/map/map1.json';
import Goomba from '../gameObjects/Goomba';



/**
 * @description a logo scene example, this is the first scene to load
 * @author © Philippe Pereira 2020
 * @export
 * @class LogoScene
 * @extends {Scene}
 */
export default class LogoScene extends Scene
{
    mario: Phaser.GameObjects.Sprite;
    map: Phaser.Tilemaps.Tilemap;
    tileset: Phaser.Tilemaps.Tileset;
    collideLayer: Phaser.Tilemaps.TilemapLayer;
    goomba: Goomba;
    constructor ()
    {
        super({
            key: 'logo'
        });
    }

    public preload (): void
    {
        // Preload assets needed for this scene and the loading scene
        
        this.load.image('logo', logo);
        this.load.spritesheet('mario', mario, {frameWidth : 16, frameHeight : 16});
        this.load.image('tiles', tiles);
        this.load.tilemapTiledJSON('map1', map1)
    }

    public create (): void
    {   
        this.mario = new Mario(this, WIDTH/2, HEIGHT/2, 'mario', 0);
        this.goomba = new Goomba(this, WIDTH/2, HEIGHT/2, 'mario', 11);

        this.map = this.make.tilemap({key : 'map1'});
        this.tileset = this.map.addTilesetImage('tiles', 'tiles');
        this.collideLayer = this.map.createLayer('collideLayer', this.tileset, 0, 0).setDepth(0);
        this.collideLayer.setCollisionByProperty ({  collides : true}) ;
        this.physics.world.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
        this.physics.add.collider(this.mario, this.collideLayer, undefined, undefined, this);
        this.physics.add.collider(this.goomba, this.collideLayer, undefined, undefined, this);

        this.cameras.main.startFollow(this.mario);
        
        
        this.anims.create({
            key: 'walk',
            frames: this.anims.generateFrameNumbers('mario', { frames: [ 0, 1, 2, 3 ] }),
            frameRate: 8,
            repeat: -1
        });

        this.anims.create({
            key: 'jump',
            frames: this.anims.generateFrameNumbers('mario', { frames: [  20, 21, 22, 23 ] }),
            frameRate: 8,
            repeat: -1
        });
        // this.mario = this.add.sprite(WIDTH/2, HEIGHT/2, 'mario')
        this.mario.anims.play('walk')
        // this.physics.add.existing(this.mario, false);
        // this.mario. anims.play('')
        console.log(this.mario)
    }


    public update(): void
    {
        // var canJump = (this.time - lastJumpedAt) > 250;
    }


}
